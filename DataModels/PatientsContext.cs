﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class PatientsContext : DbContext
    {
        public PatientsContext() : base()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            this.SaveChanges();
        }

        public DbSet<PatientEntity> Patients { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer("data source=localhost;initial catalog=Patients;" +
                "user=sa;password=23011986;App=EntityFramework;TrustServerCertificate=True")
                ;
        }
    }
}
