﻿using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;

namespace Patients.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PatientsController : ControllerBase
    {
        private readonly ILogger<PatientsController> _logger;

        public PatientsController(ILogger<PatientsController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetPatients")]
        public PatientEntity Get(int index)
        {
            return new PatientEntity 
            { 
                Id = 1, 
                BirthDate = DateTime.Now,
                LastName = "Ivanov",  
                FirstName = "Ivan", 
                MiddleName = "Ivanovich", 
                Sex = true 
            };
        }

        [HttpPost(Name = "PostPatients")]
        public PatientEntity Post(int index)
        {
            throw new NotImplementedException();
        }
    }
}
